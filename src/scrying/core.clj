(ns scrying.core
  (:gen-class)
  (:require [clojure.string :as st]
            [clojure.set :as s]))

;;;;forward declarations
(declare validate-parameters document-index parse-tokens match-recipe lazy-tokens pre-query-validation)

;;;;public API

;;;Normally I'd use a prepost condition map at the start of a
;;;function, when param validation is needed, but they don't conform
;;;to the error messages required by spec for this so manual checking is
;;;needed
(defn index!
  "places a document in the index with a given key"
  [doc-id foods]
  ;;validate the doc-id is an integer and foods are alpha numeric
  (let [[valid-id valid-foods] (validate-parameters doc-id foods)]
    (if (and valid-id valid-foods)
     ;;use set for faster comparisons, but full for user output
      (do (vswap! document-index assoc doc-id {:full (reduce (fn [v f] (conj v (str f))) [] foods)
                                          ;;keyword used for storage as kw comparison is more performant than string comp
                                          ;;would normally sanitize against double colon keywords, but all data is staying in the
                                          ;;same namespace so all comparisons will still work
                                          ;;@TODO compare timings against using keywords and strings
                                               :set (reduce (fn [s f] (conj s (str f))) #{} foods)})
          (println "index ok " doc-id)))
    (do
      (when-not valid-id
        (println "index error: Invalid doc-id, doc-id must be an integer"))
      (when-not valid-foods
        (println "index error: Invalid foods, foods must be listed using alphanumeric characters only")))))

(defn get-recipe
  "retreives a recipe by key from the document index"
  [index]
  (get-in @document-index [index]))

(defn read-recipe
  [index]
  (println (:full (get-recipe index))))

(defn query
  "performs a query against the document index"
  [q-string]
  (let [requirements (->> q-string lazy-tokens parse-tokens :required-statements)
        results (reduce (fn [m [index recipe]]
                          (if (some #{true} (match-recipe requirements (:set recipe)))
                            (conj m index) m)) #{} @document-index)]
    (if (some? requirements)
      (if (> (count results) 0)
        (println (reduce (fn [s r] (str s r " ")) "query results " results))
        (println "No results for query " q-string))
      (println "No results for query " q-string))))

;;;;Document storage implementation code
;;;volatile, as it's more performant, and we don't care about
;;;concurrancy
(def ^:private document-index (volatile! {}))

;;;validation
(defn- valid-token?
  "checks that tokens are alphanumeric"
  [token]
  (some? (re-matches #"[a-zA-Z0-9]*" (str token))))

(defn- validate-parameters
  "Validates parameters used to populate the index"
  [doc-id foods]
  [(integer? doc-id) (and (some? foods) (every? valid-token? foods))])

(defn- empty-index-check
  []
  (if (empty? @document-index)
    (do (println "query error Index is empty, please index some recipes before running a query"))
    true))

(defn- early-invalid-query-check
  "Checks if a query has two word tokens seperated by a space"
  [q-string]
  (if (nil? (re-find #"[a-zA-Z0-9]+\s+[a-zA-Z0-9]+" q-string))
    true
    (do (println "query error invalid query string, cannot have two words not sperated by | or &") false)))

(defn- pre-query-validation
  "run to validate document index and query string before execution"
  [q-string]
  (every? #{true} [(empty-index-check) (early-invalid-query-check q-string)]))
;;;;lazy token generation code
;;lazy token generator code
(def ^:private token-regex #"([a-zA-Z0-9]+|\||\&|\)|\()")

(defn- get-token
  "return the first token"
  [token-string]
  (first (re-find token-regex token-string)))

(defn- get-rest-tokens
  "return token string minus first token"
  [token-string]
  (st/replace-first token-string token-regex ""))

(defn- lazy-tokens
  "Generate a lazy sequence of tokens"
  [tokens]
  (lazy-seq (cons (get-token tokens)  (lazy-tokens (get-rest-tokens tokens)))))

;;;;State machine parser code
(defn- token-type
  "maps the tokens to keywords"
  [tkn]
  (case tkn
    "|" :or
    "&" :and
    "(" :open
    ")" :close
    nil :end
    :word))

(defn- call-next
  "convenience function for calling the next state
  is well suited for building a macro around"
  [p-tkns tkns state func]
  (func p-tkns tkns state))

(defn- state-failure
  "Function for printing errors"
  [tkn p-tkn e-type]
  (case e-type
    :invalid-token (printf "query error: Invalid query string:\nunexpected token after %s\n"
                           (apply str (conj p-tkn tkn)))
    :excess-tokens (printf "query error: Extra tokens found after expected last token\n Tokens were %s\n"
                           (apply str p-tkn))
    :state-machine (printf "query error: Error while state machine was processing token %s after tokens %s\n"
                           tkn (apply str p-tkn))
    (printf "query error: Error key not found for error type %s\n" e-type)))

(defn- get-partial-call
  "Makes a partial call to the call next function
  allows the function to be called to be specified
  after the parameters are know. Mostly for keeping the
  state machine code a bit cleaner"
  [tkn tkns p-tkns state]
  (partial call-next (conj p-tkns tkn) (rest tkns) state))

;;template for an empty sub buffer
(def fresh-sub-buffer {:current-statement []
                       ;;a buffer for the operation before the substatement
                       :former-op nil
                       :word-buffer nil})

;;template for a fresh state
(def fresh-state {;;a set of vectors which have sets of words
                  ;;a recipe must intersect with all sets of at least
                  ;;1 vector to match
                  :required-statements #{}
                  ;;a vector of sets that must match
                  :current-statement []
                  ;;buffer to hold a word token
                  :word-buffer []
                  ;; a buffer for the last operator processed
                  :former-op nil
                  :sub-buffer fresh-sub-buffer})

;;map of functions to transform state data
(def state-fn
  {:word-1 (fn [p-tkns state tkn]
             (assoc state :word-buffer tkn))
   :word-2 (fn [p-tkns state tkn]
             (assoc state :current-statement (conj (:current-statement state) #{tkn})))
   :word-3 (fn [p-tkns state tkn]
             (assoc state :sub-buffer (assoc (:sub-buffer state) :word-buffer tkn)))
   :word-4 (fn [p-tkns state tkn]
             (assoc state :sub-buffer
                    (case (:former-op state)
                      :and (assoc (->> state :sub-buffer) :current-statement (conj (->> state :sub-buffer :current-statement) #{tkn}))
                      :or (assoc (->> state :sub-buffer) :current-statement
                                 (let [sub-current-stmt (->> state :sub-buffer :current-statement last)]
                                   (replace
                                    {sub-current-stmt (conj sub-current-stmt tkn)}
                                    (->> state :sub-buffer :current-statement))))
                      (state-failure tkn p-tkns :state-machine))))
   :word-5 (fn [p-tkns state tkn]
             (assoc state :current-statement
                    (conj (:current-statement state) #{tkn})))
   :word-6 (fn [p-tkns state tkn]
             (assoc state :current-statement
                    (conj (:current-statement state) #{tkn})))

   :and-1 (fn [p-tkns state tkn]
            (case (token-type (last p-tkns))
              :close (assoc state
                            :former-op :and)
              :word (assoc state
                           :current-statement (conj (:current-statement state) #{(:word-buffer state)})
                           :word-buffer nil
                           :former-op :and)))
   :and-2 (fn [p-tkns state tkn]
            (assoc state
                   :sub-buffer (assoc (:sub-buffer state)
                                      :current-statement
                                      (conj (-> state :sub-buffer :current-statement) #{(->> state :sub-buffer :word-buffer)}))
                   :former-op :and))
   :and-3 (fn [p-tkns state tkn]
            state)

   :or-1 (fn [p-tkns state tkn]
           ;;if the sub-statement buffer is not empty, it is applied to the full statement
           (if (= :close (token-type (last p-tkns)))
             (case (->> state :former-op)
               :and (assoc state
                           :sub-buffer fresh-sub-buffer
                           :current-statement []
                           :required-statements (conj (:required-statements state)
                                                      (reduce conj (:current-statement state) (->> state :sub-buffer :current-statement)))
                           :former-op :or)
                           ;;take what was left in the sub statement buffer and place it in a new required vector
                           ;;lettuce | (pickle | tomato) -> [[#{lettuce}] [#{pickle tomato}]]
               :or (assoc state
                          :required-statements (conj (:required-statements state) (:current-statement state))
                          :current-statement []
                          :former-op :or)
               nil (assoc state
                          :required-statements (conj (:required-statements state) (->> state :sub-buffer :current-statement))
                          :sub-buffer fresh-sub-buffer
                          :former-op :or)
               (state-failure tkn p-tkns :state-machine))
            ;;add the word in the word-buffer as it's own requirement set to the current statements
             (assoc state
                    :required-statements (conj (:current-statements state) [#{(:word-buffer state)}])
                    :former-op :or
                    :word-buffer nil)))
   :or-2 (fn [p-tkns state tkn]
           (assoc state
                  :former-op :or
                  :sub-buffer (assoc (:sub-buffer state)
                                     :current-statement (conj (->> state :sub-buffer :current-statement) #{(->> state :sub-buffer :word-buffer)}))))
   :or-3 (fn [p-tkns state tkn]
           (assoc state
                  :current-statement []
                  :required-statements (conj (:required-statements state) (:current-statement state))
                  :former-op :or))
   :open (fn [p-tkns state tkn]
           (assoc state
                  :sub-buffer (assoc (:sub-buffer state) :former-op (:former-op state))))
   :close (fn [p-tkns state tkn]
            (case (:former-op state)
              :and (assoc state
                          :sub-buffer fresh-sub-buffer
                          :current-statement (case (->> state :sub-buffer :former-op)
                                               :or (->> state :sub-buffer :current-statement)
                                               :and (reduce conj (:current-statement state) (-> state :sub-buffer :current-statement))
                                               nil (->> state :sub-buffer :current-statement)))
              :or (assoc state
                         :sub-buffer fresh-sub-buffer
                         :current-statement (reduce conj (:current-statement state) (->> state :sub-buffer :current-statement)))))

   :end (fn [p-tkns state tkn]
          (assoc state
                 :current-statement []
                 :required-statements (conj (:required-statements state) (:current-statement state))))})

;;a map of functions for transitioning to different states
;;the values share enough similarity that a generator could
;;probably be made for them, but that would take more time
;;and impact readability
(def state-machine
  {:word-1 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-1 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :and (p-call (:and-1 state-machine))
                 :or (p-call (:or-1 state-machine))
                 :end (assoc new-state :required-statements #{[#{(:word-buffer new-state)}]})
                 (state-failure tkn p-tkns :invalid-token))))

   :word-2 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-2 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :end (p-call (:end state-machine))
                 :and (p-call (:and-3 state-machine))
                 (state-failure tkn p-tkns :excess-tokens))))

   :word-3 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-3 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :or (p-call (:or-2 state-machine))
                 :and (p-call (:and-2 state-machine))
                 (state-failure tkn p-tkns :invalid-token))))

   :word-4 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-4 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :close (p-call (:close state-machine))
                 (state-failure tkn p-tkns :invalid-token))))

   :word-5 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-5 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :and (p-call (:and-3 state-machine))
                 :or (p-call (:or-3 state-machine))
                 :end (p-call (:end state-machine))
                 (state-failure tkn p-tkns :invalid-token))))
   :word-6 (fn [p-tkns tkns state]
             (let [tkn (first tkns)
                   new-state ((:word-6 state-fn) p-tkns state tkn)
                   p-call (get-partial-call tkn tkns p-tkns new-state)]
               (case (token-type (second tkns))
                 :or (p-call (:or-3 state-machine))
                 :and (p-call (:and-3 state-machine))
                 :end (p-call (:end state-machine)))))

   :or-1 (fn [p-tkns tkns state]
           (let [tkn (first tkns)
                 new-state ((:or-1 state-fn) p-tkns state tkn)
                 p-call (get-partial-call tkn tkns p-tkns new-state)]
             (case (token-type (second tkns))
               :word (p-call (:word-2 state-machine))
               :open (p-call (:open state-machine))
               (state-failure tkn p-tkns :invalid-token))))

   :or-2 (fn [p-tkns tkns state]
           (let [tkn (first tkns)
                 new-state ((:or-2 state-fn) p-tkns state tkn)
                 p-call (get-partial-call tkn tkns p-tkns new-state)]
             (case (token-type (second tkns))
               :word (p-call (:word-4 state-machine))
               (state-failure tkn p-tkns :invalid-token))))

   :or-3 (fn [p-tkns tkns state]
           (let [tkn (first tkns)
                 new-state ((:or-3 state-fn) p-tkns state tkn)
                 p-call (get-partial-call tkn tkns p-tkns new-state)]
             (case (token-type (second tkns))
               :word (p-call (:word-2 state-machine))
               :open (p-call (:open state-machine))
               (state-failure tkn p-tkns :invalid-token))))

   :and-1 (fn [p-tkns tkns state]
            (let [tkn (first tkns)
                  new-state ((:and-1 state-fn) p-tkns state tkn)
                  p-call (get-partial-call tkn tkns p-tkns new-state)]
              (case (token-type (second tkns))
                :word (p-call (:word-5 state-machine))
                :open (p-call (:open state-machine))
                (state-failure tkn p-tkns :invalid-token))))

   :and-2 (fn [p-tkns tkns state]
            (let [tkn (first tkns)
                  new-state ((:and-2 state-fn) p-tkns state tkn)
                  p-call (get-partial-call tkn tkns p-tkns new-state)]
              (case (token-type (second  tkns))
                :word (p-call (:word-4 state-machine))
                (state-failure tkn p-tkns :invalid-token))))
   :and-3 (fn [p-tkns tkns state]
            (let [tkn (first tkns)
                  new-state ((:and-3 state-fn) p-tkns state tkn)
                  p-call (get-partial-call tkn tkns p-tkns new-state)]
              (case (token-type (second tkns))
                :word (p-call (:word-6 state-machine))
                (state-failure tkn p-tkns :invalid-token))))

   :open (fn [p-tkns tkns state]
           (let [tkn (first tkns)
                 new-state ((:open state-fn) p-tkns state tkn)
                 p-call (get-partial-call tkn tkns p-tkns new-state)]
             (case (token-type (first (rest tkns)))
               :word (p-call (:word-3 state-machine))
               (state-failure tkn p-tkns :invalid-token))))

   :close (fn [p-tkns tkns state]
            (let [tkn (first tkns)
                  new-state ((:close state-fn) p-tkns state tkn)
                  p-call (get-partial-call tkn tkns p-tkns new-state)]
              (case (token-type (second tkns))
                :and (p-call (:and-1 state-machine))
                :or (p-call (:or-1 state-machine))
                :end (p-call (:end state-machine))
                (state-failure tkn p-tkns :invalid-token))))

   :start (fn [tkns]
            (let [tkn (first tkns)
                  new-state fresh-state]
              (case (token-type tkn)
                :word ((:word-1 state-machine) [] tkns new-state)
                :open ((:open state-machine) [] tkns new-state)
                (state-failure tkn (seq "") :invalid-token))))
   :end (fn [p-tkns tkns state]
          ((:end state-fn) p-tkns state (first tkns)))})

(defn- parse-tokens
  "Helper function to start the state machine"
  [tokens]
  ((:start state-machine) tokens))

;;;comparison logic
(defn- match-recipe
  "matching engine for matching recipies to
  recipe requirements"
  [requirements recipe]
  ;;A requirement is a set of vectors of sets
  ;;a vector matches a recipe if all sets do not
  ;;evaluate to nil when compared with some against
  ;;the recipe's ingredient set
  ;;if any vector matches, then the recipe matches
  ;;the query
  ;;if it needs to be more performant then all
  ;;ingredient tokens in requirement and recipe should
  ;;be changed to keywords
  (let [comparitor (fn [reqs]  (some? (some recipe reqs)))
        matcher (fn [reqs] (every? true? (map comparitor reqs)))]
    (map matcher requirements)))

;;; user interface
(defn -main
  []
  (println "Available commands are: query index read-recipe help and quit")
  (let [user-input (st/trim (read-line))
        commands (st/split user-input #" ")]
    (case (first commands)
      "quit" (println "goodbye")
      "query"  (if (pre-query-validation (st/replace-first user-input #"query " ""))
                 (do (query (apply str (rest commands))) (print "\n") (-main))
                 (do (print "\n") (-main)))
      "index" (do (index! (Integer/parseInt (second commands)) (rest (rest commands))) (print "\n") (-main))
      "read-recipe" (do (read-recipe (Integer/parseInt (second commands))) (print "\n") (-main))
      "help" (do (println "query: find matching recipe indices for a query")
                 (println "index: give an index and a space delimited list of foods to search a recipe by")
                 (println "read-recipe: give an index and get back a collection of foods for that recipe")
                 (println "help: display this menu")
                 (println "quit: exit the program")
                 (print "\n")
                 (-main))
      (do (println (first commands) " is an invalid command, please use one of the commands\nquery index read-recipe help or quit\n") (-main)))))
